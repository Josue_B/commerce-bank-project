﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CommerceProject.Models;
using Microsoft.EntityFrameworkCore;


namespace CommerceProject.Controllers
{
    public class CheckoutController : Controller
    {
        private CommerceRepository repo;

        public CheckoutController(CommerceDbContext context)
        {
            repo = new CommerceRepository(context);
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult checkoutAccounts(string[] selected, string user, string datetime)
        {
            bool success = false;
            try
            {
                if (ModelState.IsValid)
                {
                    foreach(string option in selected)
                    {
                        string linkID = Guid.NewGuid().ToString();
                        repo.checkoutAccounts(option, user, datetime, linkID);
                    }                   
                    success = true;
                    
                }
            }
            catch (DbUpdateException ex)
            {
                string msg = "Try again and contact administrator if problem persists.";
                ModelState.AddModelError(ex.ToString(), msg) ;
            }

           return Json(new {success = success});
        }
    }
}
