﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CommerceProject.Models
{
    public class LockedAccountResult
    {
        public LockedAccount lockedAccount { get; set; }
        public string accountName { get; set; }
        public string accountType { get; set; }
        public string customerType { get; set; }
        public string[] selected { get; set; }

        public LockedAccountResult()
        {

        }

        public LockedAccountResult(string name, string acctType, string custType, LockedAccount locked)
        {
            accountName = name;
            accountType = acctType;
            customerType = custType;
            lockedAccount = locked;
        }
    }
}
