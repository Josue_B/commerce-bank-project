﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CommerceProject.Models
{
    public class LinkedAccount
    {
        public List<LockedAccountResult> linkedLockedAccounts { get; set; }
        public string accountsToCheckIn { get; set; }
        public string[] selected { get; set; }

        public LinkedAccount()
        {

        }

        public LinkedAccount (List<LockedAccountResult> linkedAccounts, string checkIns)
        {
            linkedLockedAccounts = linkedAccounts;
            accountsToCheckIn = checkIns;
        }
    }
}
