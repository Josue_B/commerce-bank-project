﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using Microsoft.AspNetCore.Mvc;


namespace CommerceProject.Models
{
    public partial class CommerceDbContext : DbContext
    {
        public CommerceDbContext(DbContextOptions<CommerceDbContext> options) : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<LockedAccount>(entity =>
            {
                entity.HasKey(e => e.AccountNumber);

                entity.HasIndex(e => e.AccountNumber)
                    .HasName("AK_LockedAccounts_AcctNumber")
                    .IsUnique();

                //entity.Property(e => e.Id);

                entity.Property(e => e.AccountNumber)
                    .IsRequired()
                    .HasColumnName("ACCOUNT_NUMBER")
                    .HasColumnType("varchar(18)");

                entity.Property(e => e.DateTime)
                    .HasColumnName("DATE_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.User)
                    .HasColumnName("USER")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.Link)
                .HasColumnName("LINK")
                .HasColumnType("varchar(36)");
            });

            modelBuilder.Entity<MRcfCac>(entity =>
            {
                entity.HasKey(e => new { e.RfcacfAcct, e.RfcacfAlpha })
                    .HasName("PK_M_RCF_CAC");

                entity.ToTable("M_RCF_CAC");

                entity.Property(e => e.RfcacfAcct)
                    .HasColumnName("RFCACF_ACCT")
                    .HasColumnType("varchar(18)");

                entity.Property(e => e.RfcacfAlpha)
                    .HasColumnName("RFCACF_ALPHA")
                    .HasColumnType("varchar(14)");

                entity.Property(e => e.DataDate)
                    .HasColumnName("DATA_DATE")
                    .HasColumnType("date");

                entity.Property(e => e.RfcacfAcctInst).HasColumnName("RFCACF_ACCT_INST");

                entity.Property(e => e.RfcacfAccum).HasColumnName("RFCACF_ACCUM");

                entity.Property(e => e.RfcacfCustType)
                    .HasColumnName("RFCACF_CUST_TYPE")
                    .HasColumnType("char(1)");

                entity.Property(e => e.RfcacfInst).HasColumnName("RFCACF_INST");

                entity.Property(e => e.RfcacfPrimFlag).HasColumnName("RFCACF_PRIM_FLAG");

                entity.Property(e => e.RfcacfProdcode)
                    .HasColumnName("RFCACF_PRODCODE")
                    .HasColumnType("varchar(6)");

                entity.Property(e => e.RfcacfRecType)
                    .HasColumnName("RFCACF_REC_TYPE")
                    .HasColumnType("varchar(2)");

                entity.Property(e => e.RfcacfRelNbr).HasColumnName("RFCACF_REL_NBR");

                entity.Property(e => e.RfcacfTiebrkr).HasColumnName("RFCACF_TIEBRKR");

                entity.HasOne(d => d.RfcacfAlphaNavigation)
                    .WithMany(p => p.MRcfCac)
                    .HasForeignKey(d => d.RfcacfAlpha)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("Account_to_Owner");
            });

            modelBuilder.Entity<MRcfCrf>(entity =>
            {
                entity.HasKey(e => e.RfcrffAlpha)
                    .HasName("PK_M_RCF_CRF");

                entity.ToTable("M_RCF_CRF");

                entity.Property(e => e.RfcrffAlpha)
                    .HasColumnName("RFCRFF_ALPHA")
                    .HasColumnType("varchar(14)");

                entity.Property(e => e.DataDate)
                    .HasColumnName("DATA_DATE")
                    .HasColumnType("date");

                entity.Property(e => e.RfcrffAccum).HasColumnName("RFCRFF_ACCUM");

                entity.Property(e => e.RfcrffAddrCkCd).HasColumnName("RFCRFF_ADDR_CK_CD");

                entity.Property(e => e.RfcrffAddrExt)
                    .HasColumnName("RFCRFF_ADDR_EXT")
                    .HasColumnType("varchar(40)");

                entity.Property(e => e.RfcrffAddrSupp)
                    .HasColumnName("RFCRFF_ADDR_SUPP")
                    .HasColumnType("varchar(40)");

                entity.Property(e => e.RfcrffBirthCtry)
                    .HasColumnName("RFCRFF_BIRTH_CTRY")
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.RfcrffBphone)
                    .HasColumnName("RFCRFF_BPHONE")
                    .HasColumnType("numeric");

                entity.Property(e => e.RfcrffBphoneExtn).HasColumnName("RFCRFF_BPHONE_EXTN");

                entity.Property(e => e.RfcrffBranch).HasColumnName("RFCRFF_BRANCH");

                entity.Property(e => e.RfcrffCensusTr)
                    .HasColumnName("RFCRFF_CENSUS_TR")
                    .HasColumnType("varchar(9)");

                entity.Property(e => e.RfcrffCity)
                    .HasColumnName("RFCRFF_CITY")
                    .HasColumnType("varchar(35)");

                entity.Property(e => e.RfcrffComment1)
                    .HasColumnName("RFCRFF_COMMENT1")
                    .HasColumnType("varchar(40)");

                entity.Property(e => e.RfcrffComment2)
                    .HasColumnName("RFCRFF_COMMENT2")
                    .HasColumnType("varchar(40)");

                entity.Property(e => e.RfcrffCommentFlag)
                    .HasColumnName("RFCRFF_COMMENT_FLAG")
                    .HasColumnType("char(1)");

                entity.Property(e => e.RfcrffCountryCd)
                    .HasColumnName("RFCRFF_COUNTRY_CD")
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.RfcrffCphone)
                    .HasColumnName("RFCRFF_CPHONE")
                    .HasColumnType("numeric");

                entity.Property(e => e.RfcrffCrRating)
                    .HasColumnName("RFCRFF_CR_RATING")
                    .HasColumnType("varchar(5)");

                entity.Property(e => e.RfcrffCrRatingDt).HasColumnName("RFCRFF_CR_RATING_DT");

                entity.Property(e => e.RfcrffCtznCd)
                    .HasColumnName("RFCRFF_CTZN_CD")
                    .HasColumnType("varchar(2)");

                entity.Property(e => e.RfcrffCurrEmpl)
                    .HasColumnName("RFCRFF_CURR_EMPL")
                    .HasColumnType("varchar(40)");

                entity.Property(e => e.RfcrffCustNo).HasColumnName("RFCRFF_CUST_NO");

                entity.Property(e => e.RfcrffCustType)
                    .IsRequired()
                    .HasColumnName("RFCRFF_CUST_TYPE")
                    .HasColumnType("char(1)");

                entity.Property(e => e.RfcrffCycle)
                    .HasColumnName("RFCRFF_CYCLE")
                    .HasColumnType("varchar(2)");

                entity.Property(e => e.RfcrffDeathDt).HasColumnName("RFCRFF_DEATH_DT");

                entity.Property(e => e.RfcrffDedupeFlag)
                    .HasColumnName("RFCRFF_DEDUPE_FLAG")
                    .HasColumnType("char(1)");

                entity.Property(e => e.RfcrffDeliveryPt)
                    .HasColumnName("RFCRFF_DELIVERY_PT")
                    .HasColumnType("varchar(2)");

                entity.Property(e => e.RfcrffDesc1)
                    .HasColumnName("RFCRFF_DESC1")
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.RfcrffDesc2)
                    .HasColumnName("RFCRFF_DESC2")
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.RfcrffDesc3)
                    .HasColumnName("RFCRFF_DESC3")
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.RfcrffDesc4)
                    .HasColumnName("RFCRFF_DESC4")
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.RfcrffDesc5)
                    .HasColumnName("RFCRFF_DESC5")
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.RfcrffDnsBod)
                    .HasColumnName("RFCRFF_DNS_BOD")
                    .HasColumnType("char(1)");

                entity.Property(e => e.RfcrffDob).HasColumnName("RFCRFF_DOB");

                entity.Property(e => e.RfcrffEducLvl)
                    .HasColumnName("RFCRFF_EDUC_LVL")
                    .HasColumnType("varchar(2)");

                entity.Property(e => e.RfcrffElectionCd1)
                    .HasColumnName("RFCRFF_ELECTION_CD_1")
                    .HasColumnType("char(1)");

                entity.Property(e => e.RfcrffElectionCd2)
                    .HasColumnName("RFCRFF_ELECTION_CD_2")
                    .HasColumnType("char(1)");

                entity.Property(e => e.RfcrffElectionCd3)
                    .HasColumnName("RFCRFF_ELECTION_CD_3")
                    .HasColumnType("char(1)");

                entity.Property(e => e.RfcrffElectionDt1).HasColumnName("RFCRFF_ELECTION_DT_1");

                entity.Property(e => e.RfcrffElectionDt2).HasColumnName("RFCRFF_ELECTION_DT_2");

                entity.Property(e => e.RfcrffElectionDt3).HasColumnName("RFCRFF_ELECTION_DT_3");

                entity.Property(e => e.RfcrffEmplDt).HasColumnName("RFCRFF_EMPL_DT");

                entity.Property(e => e.RfcrffEthnic)
                    .HasColumnName("RFCRFF_ETHNIC")
                    .HasColumnType("char(1)");

                entity.Property(e => e.RfcrffFstCtac)
                    .HasColumnName("RFCRFF_FST_CTAC")
                    .HasColumnType("varchar(5)");

                entity.Property(e => e.RfcrffFstCtacDt).HasColumnName("RFCRFF_FST_CTAC_DT");

                entity.Property(e => e.RfcrffGlRptCd)
                    .HasColumnName("RFCRFF_GL_RPT_CD")
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.RfcrffHphone)
                    .HasColumnName("RFCRFF_HPHONE")
                    .HasColumnType("numeric");

                entity.Property(e => e.RfcrffIncClass)
                    .HasColumnName("RFCRFF_INC_CLASS")
                    .HasColumnType("varchar(5)");

                entity.Property(e => e.RfcrffInst).HasColumnName("RFCRFF_INST");

                entity.Property(e => e.RfcrffLastCipActyDt).HasColumnName("RFCRFF_LAST_CIP_ACTY_DT");

                entity.Property(e => e.RfcrffLegalCd)
                    .HasColumnName("RFCRFF_LEGAL_CD")
                    .HasColumnType("char(1)");

                entity.Property(e => e.RfcrffLstCtac)
                    .HasColumnName("RFCRFF_LST_CTAC")
                    .HasColumnType("varchar(5)");

                entity.Property(e => e.RfcrffLstCtacDt).HasColumnName("RFCRFF_LST_CTAC_DT");

                entity.Property(e => e.RfcrffMaidenName)
                    .HasColumnName("RFCRFF_MAIDEN_NAME")
                    .HasColumnType("varchar(40)");

                entity.Property(e => e.RfcrffMailCd)
                    .HasColumnName("RFCRFF_MAIL_CD")
                    .HasColumnType("char(1)");

                entity.Property(e => e.RfcrffMailing)
                    .HasColumnName("RFCRFF_MAILING")
                    .HasColumnType("varchar(40)");

                entity.Property(e => e.RfcrffMaintDt).HasColumnName("RFCRFF_MAINT_DT");

                entity.Property(e => e.RfcrffMaintType).HasColumnName("RFCRFF_MAINT_TYPE");

                entity.Property(e => e.RfcrffMarital)
                    .HasColumnName("RFCRFF_MARITAL")
                    .HasColumnType("char(1)");

                entity.Property(e => e.RfcrffMktgCd)
                    .HasColumnName("RFCRFF_MKTG_CD")
                    .HasColumnType("varchar(12)");

                entity.Property(e => e.RfcrffName)
                    .HasColumnName("RFCRFF_NAME")
                    .HasColumnType("varchar(40)");

                entity.Property(e => e.RfcrffNameFirst)
                    .HasColumnName("RFCRFF_NAME_FIRST")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.RfcrffNameLast)
                    .HasColumnName("RFCRFF_NAME_LAST")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.RfcrffNameMidd1)
                    .HasColumnName("RFCRFF_NAME_MIDD1")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.RfcrffNameMidd2)
                    .HasColumnName("RFCRFF_NAME_MIDD2")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.RfcrffNamePrefix)
                    .HasColumnName("RFCRFF_NAME_PREFIX")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.RfcrffNameSuffix1)
                    .HasColumnName("RFCRFF_NAME_SUFFIX1")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.RfcrffNameSuffix2)
                    .HasColumnName("RFCRFF_NAME_SUFFIX2")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.RfcrffOccupCd)
                    .HasColumnName("RFCRFF_OCCUP_CD")
                    .HasColumnType("varchar(5)");

                entity.Property(e => e.RfcrffOfficer)
                    .HasColumnName("RFCRFF_OFFICER")
                    .HasColumnType("varchar(9)");

                entity.Property(e => e.RfcrffOfficer2)
                    .HasColumnName("RFCRFF_OFFICER2")
                    .HasColumnType("varchar(9)");

                entity.Property(e => e.RfcrffOwnRentCd)
                    .HasColumnName("RFCRFF_OWN_RENT_CD")
                    .HasColumnType("char(1)");

                entity.Property(e => e.RfcrffPin1).HasColumnName("RFCRFF_PIN1");

                entity.Property(e => e.RfcrffPin2).HasColumnName("RFCRFF_PIN2");

                entity.Property(e => e.RfcrffPostal)
                    .HasColumnName("RFCRFF_POSTAL")
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.RfcrffPriLang)
                    .HasColumnName("RFCRFF_PRI_LANG")
                    .HasColumnType("varchar(2)");

                entity.Property(e => e.RfcrffPrivacyCd)
                    .HasColumnName("RFCRFF_PRIVACY_CD")
                    .HasColumnType("char(1)");

                entity.Property(e => e.RfcrffPrivacyDt).HasColumnName("RFCRFF_PRIVACY_DT");

                entity.Property(e => e.RfcrffProv)
                    .HasColumnName("RFCRFF_PROV")
                    .HasColumnType("varchar(2)");

                entity.Property(e => e.RfcrffRecType)
                    .HasColumnName("RFCRFF_REC_TYPE")
                    .HasColumnType("varchar(2)");

                entity.Property(e => e.RfcrffReferralCd)
                    .HasColumnName("RFCRFF_REFERRAL_CD")
                    .HasColumnType("char(1)");

                entity.Property(e => e.RfcrffReserved)
                    .HasColumnName("RFCRFF_RESERVED")
                    .HasColumnType("varchar(12)");

                entity.Property(e => e.RfcrffRestrict)
                    .HasColumnName("RFCRFF_RESTRICT")
                    .HasColumnType("char(1)");

                entity.Property(e => e.RfcrffRiskCd)
                    .HasColumnName("RFCRFF_RISK_CD")
                    .HasColumnType("char(1)");

                entity.Property(e => e.RfcrffRouteCd)
                    .HasColumnName("RFCRFF_ROUTE_CD")
                    .HasColumnType("varchar(4)");

                entity.Property(e => e.RfcrffSeniorExec)
                    .HasColumnName("RFCRFF_SENIOR_EXEC")
                    .HasColumnType("char(1)");

                entity.Property(e => e.RfcrffSensitiveInd)
                    .HasColumnName("RFCRFF_SENSITIVE_IND")
                    .HasColumnType("char(1)");

                entity.Property(e => e.RfcrffServiceLvl)
                    .HasColumnName("RFCRFF_SERVICE_LVL")
                    .HasColumnType("char(1)");

                entity.Property(e => e.RfcrffSex)
                    .HasColumnName("RFCRFF_SEX")
                    .HasColumnType("char(1)");

                entity.Property(e => e.RfcrffStAddr)
                    .HasColumnName("RFCRFF_ST_ADDR")
                    .HasColumnType("varchar(40)");

                entity.Property(e => e.RfcrffState)
                    .HasColumnName("RFCRFF_STATE")
                    .HasColumnType("varchar(2)");

                entity.Property(e => e.RfcrffStatus)
                    .HasColumnName("RFCRFF_STATUS")
                    .HasColumnType("char(1)");

                entity.Property(e => e.RfcrffTaxId)
                    .HasColumnName("RFCRFF_TAX_ID")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.RfcrffTaxStatus)
                    .HasColumnName("RFCRFF_TAX_STATUS")
                    .HasColumnType("char(1)");

                entity.Property(e => e.RfcrffTaxStatusDt).HasColumnName("RFCRFF_TAX_STATUS_DT");

                entity.Property(e => e.RfcrffTiebrkr).HasColumnName("RFCRFF_TIEBRKR");

                entity.Property(e => e.RfcrffTinCertCd)
                    .HasColumnName("RFCRFF_TIN_CERT_CD")
                    .HasColumnType("char(1)");

                entity.Property(e => e.RfcrffTinCertDt).HasColumnName("RFCRFF_TIN_CERT_DT");

                entity.Property(e => e.RfcrffTinType)
                    .HasColumnName("RFCRFF_TIN_TYPE")
                    .HasColumnType("char(1)");

                entity.Property(e => e.RfcrffUserCd1Rest)
                    .HasColumnName("RFCRFF_USER_CD1_REST")
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.RfcrffUserCd2)
                    .HasColumnName("RFCRFF_USER_CD2")
                    .HasColumnType("varchar(5)");

                entity.Property(e => e.RfcrffUserCd3)
                    .HasColumnName("RFCRFF_USER_CD3")
                    .HasColumnType("varchar(5)");

                entity.Property(e => e.RfcrffUserCd4)
                    .HasColumnName("RFCRFF_USER_CD4")
                    .HasColumnType("varchar(5)");

                entity.Property(e => e.RfcrffUserDemo)
                    .HasColumnName("RFCRFF_USER_DEMO")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.RfcrffZip4).HasColumnName("RFCRFF_ZIP4");

                entity.Property(e => e.RfcrffZip5).HasColumnName("RFCRFF_ZIP5");
            });
        }

        public virtual DbSet<LockedAccount> LockedAccount { get; set; }
        public virtual DbSet<MRcfCac> MRcfCac { get; set; }
        public virtual DbSet<MRcfCrf> MRcfCrf { get; set; }
    }
}